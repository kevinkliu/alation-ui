const SuggestionType = Object.freeze({
  TABLE: 'TABLE',
  COLUMN: 'COLUMN',
  KEYWORD: 'KEYWORD',
});

class Suggestion {
  constructor(type, snippet) {
    this.type = type;
    this.snippet = snippet;
  }
}

const StartingKeywords = [
  new Suggestion(SuggestionType.KEYWORD, 'SELECT'),
  new Suggestion(SuggestionType.KEYWORD, 'CREATE'),
  new Suggestion(SuggestionType.KEYWORD, 'ALTER'),
  new Suggestion(SuggestionType.KEYWORD, 'INSERT'),
  new Suggestion(SuggestionType.KEYWORD, 'UPDATE'),
];

class Suggester {
  constructor(tables) {
    this.tables = tables;
  }

  getSuggestions(statement, cursorPosition) {
    const select = statement.getSelectClause();
    if (select) {
      if (select.inColumnNamePosition(cursorPosition)) {
        return Object.keys(this.tables).reduce((suggestions, tableName) => {
          if (!select.tables || select.tables.length === 0) {
            Object.keys(this.tables).forEach(t =>
              this.tables[t].forEach(c =>
                  suggestions.push(new Suggestion(SuggestionType.COLUMN, c));
                ),
            );
          } else if (select.tables.includes(tableName)) {
            this.tables[tableName].forEach(c =>
              suggestions.push(new Suggestion(SuggestionType.COLUMN, c)),
            );
          }
          return suggestions;
        }, []);
      } else if (select.inTableNamePosition(cursorPosition)) {
        return Object.keys(this.tables).reduce((suggestions, tableName) => {
          if (this.tables[tableName].find(c => select.columns.includes(c))) {
            suggestions.push(new Suggestion(SuggestionType.TABLE, tableName));
          }
          return suggestions;
        }, []);
      }
    }
    return StartingKeywords;
  }
}

export { Suggester, Suggestion, SuggestionType };
